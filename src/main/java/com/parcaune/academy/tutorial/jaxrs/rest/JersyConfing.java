package com.parcaune.academy.tutorial.jaxrs.rest;


import com.parcaune.academy.tutorial.jaxrs.endpoint.PingEndPoint;
import com.parcaune.academy.tutorial.jaxrs.endpoint.StudentEndpoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/rest")
public class JersyConfing extends ResourceConfig {

    public JersyConfing(){
        register(PingEndPoint.class);
        register(StudentEndpoint.class);
    }

}
