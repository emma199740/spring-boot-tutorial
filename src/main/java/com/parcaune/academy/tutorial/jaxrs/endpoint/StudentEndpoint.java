package com.parcaune.academy.tutorial.jaxrs.endpoint;

import com.parcaune.academy.tutorial.jaxrs.domain.Student;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;

@Component
@Path("/students")
public class StudentEndpoint {

    static HashMap<Integer, Student> studentList = new HashMap<>();

    static {
        //Initializing HashMap with some Objects
        Student student1 = new Student(1, "Dadem", "Emmanuel");
        Student student2 = new Student(2, "Ali", "Bala");
        studentList.put(student1.getId(), student1);
        studentList.put(student2.getId(), student2);
    }

    @GET
    @Path("/ping")
    @Produces(MediaType.TEXT_PLAIN)
    public String ping(){
        return "StudentEndpoint works!!  :D";
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public HashMap<Integer, Student> getAll(){
        System.out.println("Getting all Students");
        return studentList;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Student student) {
        System.out.println("Adding Student with ID" + student.getId() );
        studentList.put(student.getId(), student);
        return Response.ok(student).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(Student student, @PathParam("id") Integer id) {
        System.out.println("Updating Student with ID " + student.getId());
        studentList.replace(student.getId(), student);
        return Response.ok(student).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteById(@PathParam("id") Integer id){
        System.out.println("Deleting Student with ID " + id );
        studentList.remove(id);
        return Response.ok("deleted").build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Integer id){
        System.out.println("Getting Student with ID " + id );
        Student student = studentList.get(id);
        if (student == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(student).build();
    }

    @GET
    @Path("/querry")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Student> getWithQuerry(@QueryParam("start") Integer start, @QueryParam("limit") Integer limit){
        System.out.println("Getting Student with querryParams" );
        ArrayList<Student> resultList = new ArrayList<>();
        for (int i = start; i <= limit ; i++){
            resultList.add(studentList.get(i));
        }
        return  resultList;
    }
}
