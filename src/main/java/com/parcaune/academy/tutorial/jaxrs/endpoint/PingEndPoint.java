package com.parcaune.academy.tutorial.jaxrs.endpoint;


import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Component
@Path("/ping")
public class PingEndPoint {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String index(){
        return "JAX-RS:: It's works!! :D";
    }


    @GET
    @Path("/{message}")
    @Produces(MediaType.TEXT_PLAIN)
    public String pingWithParams(@PathParam("message") String message){
        return "Wellcome " + message;
    }


}
